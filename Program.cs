﻿using System;

namespace EvenFibonacciNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int next = 0;
            int first = 1;
            int second = 2;

            Console.WriteLine(first);
            Console.WriteLine(second);

            while ((first + second) < 4000000)
            {
                next = first + second;
                first = second;
                second = next;

                Console.WriteLine(next);
            }
        }
    }
}
